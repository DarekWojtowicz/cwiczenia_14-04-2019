import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Student s1 = new Student("Wika", "Wojtowicz");
        Student s2 = new Student("Waldek", "Wojtowicz");

        s1.addSubjectForStudent(ClassesEnum.ENGLISH);
        s1.addSubjectForStudent(ClassesEnum.MATH);
        s1.addSubjectForStudent(ClassesEnum.BIOLOGY);
        s1.addSubjectForStudent(ClassesEnum.JAVA);
        s1.addSubjectForStudent(ClassesEnum.POLISH);

        s1.addGrade(ClassesEnum.ENGLISH, 4);
        s1.addGrade(ClassesEnum.MATH, 5);
        s1.addGrade(ClassesEnum.BIOLOGY, 3);
        s1.addGrade(ClassesEnum.POLISH, 5);


        s2.addSubjectForStudent(ClassesEnum.MATH);
        s2.addSubjectForStudent(ClassesEnum.ENGLISH);
        s2.addSubjectForStudent(ClassesEnum.BIOLOGY);
        s2.addSubjectForStudent(ClassesEnum.JAVA);

       s2.addGrade(ClassesEnum.JAVA, 4);
//        s2.addGrade(ClassesEnum.MATH, 5);
//        s2.addGrade(ClassesEnum.ENGLISH, 4);

        System.out.println(s1);
        System.out.println(s2);
        System.out.println();

        System.out.println("Waldka średnia: " + s1.getAverageOfStudentGrades());
        System.out.println("Wiki średnia  : " +s2.getAverageOfStudentGrades());

        s1.showStudentSubjects();
        s2.showStudentSubjects();

        System.out.println("Lista zajęć Waldka bez ocen: " + s1.subjectsWithoutGrade());
        System.out.println("Lista zajęć Wiki bez ocen  : " + s2.subjectsWithoutGrade());

    }
}
