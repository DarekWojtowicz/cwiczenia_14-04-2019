import java.util.Comparator;

public class PlayerShoesComparator implements Comparator<Player> {

    @Override
    public int compare(Player o1, Player o2) {
        return (int) (o1.getRozmiarButa() - o2.getRozmiarButa());
    }
}
