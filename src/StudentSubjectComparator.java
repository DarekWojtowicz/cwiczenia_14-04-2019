import java.util.Comparator;

public class StudentSubjectComparator implements Comparator <ClassesEnum> {

    @Override

    public int compare(ClassesEnum classesEnum, ClassesEnum t1) {
        return classesEnum.toString().compareTo(t1.toString());
    }
}
