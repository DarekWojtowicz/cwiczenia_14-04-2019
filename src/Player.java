public class Player implements Comparable <Player>{
    private String name;
    private String lastname;
    private Integer overall;
    private Integer age;
    private String nazwaKlubu;
    private Double rozmiarButa;

    public Player(String name, String lastname, Integer overall) {
        this.name = name;
        this.lastname = lastname;
        this.overall = overall;
    }

    public Player(String name, String lastname, Integer overall, Integer age, String nazwaKlubu, Double rozmiarButa) {
        this.name = name;
        this.lastname = lastname;
        this.overall = overall;
        this.age = age;
        this.nazwaKlubu = nazwaKlubu;
        this.rozmiarButa = rozmiarButa;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Integer getOverall() {
        return overall;
    }

    public void setOverall(Integer overall) {
        this.overall = overall;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getNazwaKlubu() {
        return nazwaKlubu;
    }

    public void setNazwaKlubu(String nazwaKlubu) {
        this.nazwaKlubu = nazwaKlubu;
    }

    public Double getRozmiarButa() {
        return rozmiarButa;
    }

    public void setRozmiarButa(Double rozmiarButa) {
        this.rozmiarButa = rozmiarButa;
    }

    @Override
    public int compareTo(Player o) {
        return this.getOverall() - o.getOverall();
    }

    @Override
    public String toString() {
        return "name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", overall=" + overall +
                ", age=" + age +
                ", nazwaKlubu='" + nazwaKlubu + '\'' +
                ", rozmiarButa=" + rozmiarButa +
                '}';
    }
}
