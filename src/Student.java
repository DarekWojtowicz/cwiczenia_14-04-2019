import java.util.*;

public class Student {
    private String name;
    private String lastName;
    private Integer studentId;
    private List<ClassesEnum> listOfStudentSubjects;
    private Map<ClassesEnum, Integer> studentGrades;
    private Random random= new Random();
    private Double avarage = 0.00;
    public StudentSubjectComparator subject = new StudentSubjectComparator();



    public Student(String name, String lastName){
        this.name = name;
        this.lastName = lastName;
        this.studentId = random.nextInt((99999 - 10000) + 1) + 10000;
        this.listOfStudentSubjects = new ArrayList<>();
        this.studentGrades = new HashMap<>();
    }

    public void addSubjectForStudent(ClassesEnum subject){
        //dodajemy do listy przedmiotów listOfS0tudentGrades
        this.listOfStudentSubjects.add(subject);
    }

    public void addGrade(ClassesEnum subject, Integer subjectGrade){
        if(this.listOfStudentSubjects.contains(subject)){
            this.studentGrades.put(subject, subjectGrade);
        }else {
            System.out.println("Student nie uczeszcza na takie zajęcia!");
        }
        //sprawdzenie czy student uczeszcza na dany przedmiot
        //przypisanie oceny do przedmiotu, podanie jej do mapy
    }

    public void showStudentSubjects(){
        //print whole listOfStudentSubjects list

        Collections.sort(listOfStudentSubjects, subject);
        for(ClassesEnum tmp : listOfStudentSubjects){
            System.out.print(tmp.toString() + " ");
        }
        System.out.println();
    }


    public Double getAverageOfStudentGrades(){
        //na mapie mamy właściwośc toEntrySet , po niej iterujemy wyciągając wszyskie oceny
        // i wyciągając z nich średnią
        //ZWRACAMY WYNIK w postaci double
        for(ClassesEnum zajecia : this.studentGrades.keySet()){
            if(this.studentGrades.containsKey(zajecia)){
                avarage += (double) this.studentGrades.get(zajecia);
            }
        }
        if(this.studentGrades.size() == 0){
            return 0.0;
        }else {
            return avarage / this.studentGrades.size();
        }
    }

    public List<ClassesEnum> subjectsWithoutGrade() {
        //wyciągamy wszystkie przedmioty z mampy i zwracamy liste przedmiotow, ktore maja wartosc null
        List <ClassesEnum> zajeciaBezOcen = new ArrayList<>();
        for(ClassesEnum zajecia : listOfStudentSubjects){
            if(this.studentGrades.get(zajecia) == null){
                zajeciaBezOcen.add(zajecia);
            }
        }
        return zajeciaBezOcen;
    }


    @Override
    public String toString() {
        Collections.sort(listOfStudentSubjects, subject);


        return  "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", studentId=" + studentId +
                ", listOfStudentSubjects=" + listOfStudentSubjects +
                ", studentGrades=" + studentGrades;
    }

//    public String getListOfStudentSubjects() {
//        return listOfStudentSubjects.toString();
//    }
//
    public int getStudentGrades(ClassesEnum zajecia) {
        return studentGrades.get(zajecia);

    }
}